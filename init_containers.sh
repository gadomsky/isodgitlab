#!/bin/bash 

#ldap-server
docker run --name ldap-service --hostname ldap-service --detach osixia/openldap:1.1.7

#phpldapadmip
docker run -p 6443:443 --name phpldapadmin-service --hostname phpldapadmin-service1 --link ldap-service:ldap-host --env PHPLDAPADMIN_LDAP_HOSTS=ldap-host --detach osixia/phpldapadmin:0.6.12

#gitlab
docker run --detach \
    --hostname isodgitlab \
	--link ldap-service:ldap-host \
    --env GITLAB_OMNIBUS_CONFIG=" \
	gitlab_rails['ldap_enabled'] = true ; \
	gitlab_rails['ldap_host'] = `echo $"LDAP_HOST_PORT_389_TCP_ADDR"` ; \
	gitlab_rails['ldap_port'] = 389 ; \
	gitlab_rails['ldap_uid'] = 'uid' ; \
	gitlab_rails['ldap_method'] = 'plain' ; \
	gitlab_rails['ldap_bind_dn'] = 'CN=admin,DC=example,DC=org' ; \
	gitlab_rails['ldap_password'] = 'admin' ; \
	gitlab_rails['ldap_active_directory'] = false ; \
	gitlab_rails['ldap_allow_username_or_email_login'] = true ; \
	gitlab_rails['ldap_base'] = 'DC=example,DC=org' ; \
	" \
    --publish 9443:443 --publish 9980:80 --publish 9922:22 \
    --name gitlab \
    --restart always \
    gitlab/gitlab-ce:latest

