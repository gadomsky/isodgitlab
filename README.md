# **PRZYGOTOWANIE ŚRODOWISKA** #

1. Inicjalizacja kontenerów (openldap-server, phpldapadmin, gitlab)
>./init_containers.sh

2. Dodanie czterech testowych użytkowników do LDAP'a
>./init_ldap_users.sh

3. Założenie konta admnistratora poprzez GUI Gitlaba oraz dodanie tokenu (który jest dostępny tu GITLAB_URL//profile/account) do pliku konfiguracyjnego gitlab.properties (src/main/resources/gitlab.properties)

4. Uruchomienie aplikacji (do developmentu można wykorzystać wbduowany server Jetty, który jest dostępny poprzez plugin w Mavenie)
>mvn jetty:run

5. Aplikację mozna uruchomic także w dowolnym kontenerze serwletów (np Tomcat) poprzez osadzenie w nim pliku war:
>mvn install

Paczka war znajduje się w target/isodgitlab-1.0-SNAPSHOT.war