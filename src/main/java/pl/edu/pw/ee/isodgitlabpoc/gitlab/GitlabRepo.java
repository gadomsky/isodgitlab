package pl.edu.pw.ee.isodgitlabpoc.gitlab;

/**
 * Created by Krzysiek on 19.01.2017.
 */
public class GitlabRepo {
    private final String name;
    private final String description;
    private Long id;

    public GitlabRepo(Long id, String name, String description) {
        this.name = name;
        this.id = id;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "GitlabRepo{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", id=" + id +
                '}';
    }

    public void setId(long id) {
        this.id = id;
    }
}
