package pl.edu.pw.ee.isodgitlabpoc.gitlab;

import java.util.Map;

public final class User {
    private long id;
    private String username;
    private String email;
    private String name;
    private boolean isAdmin;
    private boolean active;
    private Map<Long, String> keys;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Map<Long, String> getKeys() {
        return keys;
    }

    public void setKeys(Map<Long, String> keys) {
        this.keys = keys;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", isAdmin=" + isAdmin +
                ", active=" + active +
                ", keys=" + keys +
                '}';
    }
}
