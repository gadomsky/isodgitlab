package pl.edu.pw.ee.isodgitlabpoc.gitlab;

/**
 * Created by Krzysiek on 10.01.2017.
 */
public class GitlabApiException extends Exception{
    public final String msg;

    public GitlabApiException(String msg) {
        this.msg = msg;
    }
}