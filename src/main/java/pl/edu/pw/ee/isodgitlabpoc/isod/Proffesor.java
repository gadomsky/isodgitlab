package pl.edu.pw.ee.isodgitlabpoc.isod;

import java.io.Serializable;

/**
 * Created by Krzysiek on 27.01.2017.
 */
public class Proffesor implements Serializable {
    private static final long serialVersionUID = 1L;
    private String login;
    private String email;
    private String name;
    private String lastName;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Proffesor(String name, String lastName, String email, String login) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.login = login;
    }

    @Override
    public String toString() {
        return name + " " + lastName;
    }

}
