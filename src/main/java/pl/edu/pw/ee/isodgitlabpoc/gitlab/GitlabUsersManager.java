package pl.edu.pw.ee.isodgitlabpoc.gitlab;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class GitlabUsersManager {
    final Logger log = LogManager.getLogger(this.getClass());
    private GitlabApiConnection gitlabApiConnection;

    GitlabUsersManager(GitlabApiConnection gitlabApiConnection) {
        this.gitlabApiConnection = gitlabApiConnection;
    }

    public User createUser(String userJsonString) throws GitlabApiException {
        log.info("Creating user " + userJsonString);
        User u = getUserFromJsonObject(this.gitlabApiConnection.postRequest("users", userJsonString));
        return u;
    }

    public void deploySshKey(User u, String key) throws GitlabApiException {
        Map<String, String> jsonMap = new HashMap<>();
        jsonMap.put("title", "Klucz ssh - " + u.getName());
        jsonMap.put("key", key);
        JSONObject json = new JSONObject(jsonMap);

        log.info("Deploying ssh key [" + key + "] for user " + u.toString());
        this.gitlabApiConnection.postRequest("users/" + u.getId() + "/keys", json.toJSONString());
    }

    @SuppressWarnings("unchecked")
    public Map<Long, String> getUserSshKeys(User u) throws GitlabApiException {
        log.info("Getting ssh keys for user " + u.toString());

        JSONArray jsonKeys = this.gitlabApiConnection.getRequestArray("users/" + u.getId() + "/keys");
        Map<Long, String> idToKey = new HashMap<>();
        jsonKeys.forEach(o -> idToKey.put((Long) ((JSONObject) o).get("id"), (String) ((JSONObject) o).get("key")));
        return idToKey;
    }

    public void suspendUser(User u) throws GitlabApiException {
        if (!u.isAdmin()) {
            log.info("Suspending user " + u.toString());
            this.gitlabApiConnection.putRequest("users/" + u.getId() + "/block", "");
        }
    }

    public void unsuspendUser(User u) throws GitlabApiException {
        log.info("Unsuspending user " + u.toString());
        this.gitlabApiConnection.putRequest("users/" + u.getId() + "/unblock", "");
    }

    public Map<String, User> getAllUsers() throws GitlabApiException {
        log.info("Getting all users");

        Map<String, User> usernameToGitlabUser = new HashMap<>();

        this.gitlabApiConnection.getRequestWithPagination("users").stream().forEach(o -> {
            User u = getUserFromJsonObject((JSONObject) o);
            usernameToGitlabUser.put(u.getUsername(), u);
        });
        return usernameToGitlabUser;
    }


    public void deleteUserKey(long userId, long keyId) throws GitlabApiException {
        log.info("Deleting ssh key [" + userId + "] for user with id: " + keyId);
        this.gitlabApiConnection.deleteRequest("users/" + userId + "/keys/" + keyId);
    }

    public boolean deleteUsersKeys(User u) {
        final boolean[] ret = {true};
        u.getKeys().entrySet().stream().forEach(entry -> {
            try {
                deleteUserKey(u.getId(), entry.getKey());
            } catch (GitlabApiException e) {
                ret[0] = false;
            }
        });
        return ret[0];
    }

    private User getUserFromJsonObject(JSONObject o) {
        User u = new User();
        u.setId((long) o.get("id"));
        u.setUsername((String) o.get("username"));
        u.setName((String) o.get("name"));
        u.setEmail((String) o.get("email"));
        u.setAdmin((boolean) o.get("is_admin"));
        u.setActive(((String) o.get("state")).equals("active"));
        return u;
    }

    public boolean isUserActive(String login) throws GitlabApiException {
        return this.gitlabApiConnection.getRequestWithPaginationWithQuery("users", "username", login)
                .stream()
                .filter(o -> (((JSONObject) o).get("username").equals(login)))
                .filter(o -> (((JSONObject) o).get("state").equals("active")))
                .findAny().isPresent();
    }

    public Long getUserId(String login) throws GitlabApiException {
        return (Long) this.gitlabApiConnection.getRequestWithPaginationWithQuery("users", "username", login)
                .stream()
                .filter(o -> (((JSONObject) o).get("username").equals(login)))
                .filter(o -> (((JSONObject) o).get("state").equals("active")))
                .map( o -> (((JSONObject) o).get("id")))
                .findAny().orElseGet(() -> null);
    }


}
