package pl.edu.pw.ee.isodgitlabpoc;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import pl.edu.pw.ee.isodgitlabpoc.gitlab.GitlabHelper;
import pl.edu.pw.ee.isodgitlabpoc.isod.IsodCourseMock;
import pl.edu.pw.ee.isodgitlabpoc.isod.IsodMockService;
import pl.edu.pw.ee.isodgitlabpoc.panel.IsodGitlabModule;

public class IsodMockPage extends WebPage {
    private static final long serialVersionUID = 1L;

    public IsodMockPage(final PageParameters parameters) {
        GitlabHelper gitlab = GitlabHelper.getInstance();
        String message = "Połączenie z Gitlabem jest ";
        try {
            if (gitlab.amIAdmin()) {
                message = message + "poprawne.";
            } else {
                message = message + "poprawne, ale zostało skonfigurowane za pomocą konta z niewystarczającymi uprawnieniami";
            }
        } catch (Exception e) {
            message = message + "niepoprawne.";
            e.printStackTrace();
        }
        add(new Label("message", message));


        IsodCourseMock isodCourseMock1 = IsodMockService.getInstance().getIsodCourseMocks().get(0);
        Link<Void> linkWithLabel = new Link<Void>("isodCourseMock1") {
            @Override
            public void onClick() {
                IsodGitlabModule page = new IsodGitlabModule();
                page.setSelectedCourse(isodCourseMock1.getSemesterSymbol() + "/" + isodCourseMock1.getTitle());
                setResponsePage(page);
            }
        };
        linkWithLabel.setBody(Model.of("Moduł dla " + isodCourseMock1.getSemesterSymbol() + "/" + isodCourseMock1.getTitle()));
        add(linkWithLabel);


        IsodCourseMock isodCourseMock2 = IsodMockService.getInstance().getIsodCourseMocks().get(1);
        Link<Void> linkWithLabel2 = new Link<Void>("isodCourseMock2") {
            @Override
            public void onClick() {
                IsodGitlabModule page = new IsodGitlabModule();
                page.setSelectedCourse(isodCourseMock2.getSemesterSymbol() + "/" + isodCourseMock2.getTitle());
                setResponsePage(page);
            }
        };
        linkWithLabel2.setBody(Model.of("Moduł dla " + isodCourseMock2.getSemesterSymbol() + "/" + isodCourseMock2.getTitle()));
        add(linkWithLabel2);
    }

}
