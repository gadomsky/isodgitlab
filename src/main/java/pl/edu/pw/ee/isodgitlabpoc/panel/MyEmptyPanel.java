package pl.edu.pw.ee.isodgitlabpoc.panel;

import org.apache.wicket.markup.html.panel.Panel;

import java.io.Serializable;

/**
 * Created by Krzysiek on 21.01.2017.
 */
public class MyEmptyPanel extends Panel implements Serializable{
    private static final long serialVersionUID = 1L;

    public MyEmptyPanel(String id) {
        super(id);
    }
}
