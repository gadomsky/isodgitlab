package pl.edu.pw.ee.isodgitlabpoc.isod;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Krzysiek on 14.01.2017.
 */
public class IsodCourseMock implements Serializable {
    private static final long serialVersionUID = 1L;

    private final Set<Student> students;
    private final List<ExamWithRepo> exams;

    private final String subjectSymbol;
    private final String title;
    private final String semesterSymbol;
    private final Proffesor proffesor;

    public IsodCourseMock(Set<Student> students, String subjectSymbol, String title, String semesterSymbol, Proffesor proffesor) {
        this.students = students;
        this.subjectSymbol = subjectSymbol;
        this.title = title;
        this.semesterSymbol = semesterSymbol;
        this.proffesor = proffesor;
        this.exams = new ArrayList<>();
    }

    public Set<Student> getStudents() {
        return students;
    }

    public List<ExamWithRepo> getExams() {
        return exams;
    }

    public String getSubjectSymbol() {
        return subjectSymbol;
    }

    public String getSemesterSymbol() {
        return semesterSymbol;
    }

    public Proffesor getProffesor() {
        return proffesor;
    }

    public String getTitle() {
        return title;
    }

    public void addExam(String title) {
        exams.add(new ExamWithRepo(title, this));
    }

}
