package pl.edu.pw.ee.isodgitlabpoc.isod;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IsodMockService implements Serializable {

    private static final class Holder{
        static final IsodMockService INSTANCE = new IsodMockService();
    }
    private static final long serialVersionUID = 1L;
    private List<IsodCourseMock> isodCourseMocks = new ArrayList<>();

    public static IsodMockService getInstance() {
        return Holder.INSTANCE;
    }

    private IsodMockService() {
        initMocks();
    }

    private void initMocks() {
        StudentService studentService = StudentService.getInstance();

        Set<Student> studentList1 = new HashSet<Student>() {{
            add(studentService.getStudentByLogin("kgadomski"));
            add(studentService.getStudentByLogin("itunel"));
            add(studentService.getStudentByLogin("jnowak"));
            add(studentService.getStudentByLogin("kmarek"));
            add(studentService.getStudentByLogin("probak"));
            add(studentService.getStudentByLogin("anowacka"));
            add(studentService.getStudentByLogin("krychter"));
        }};

        Proffesor proffesor1 = new Proffesor("Krzysztof", "Hryniow", "khryniow@example.org", "khryniow");

        IsodCourseMock isodCourseMock1 = new IsodCourseMock(
                studentList1,
                "1DI2203:A",
                "Zaawansowane systemy baz danych, Laboratoria",
                "2016Z",
                proffesor1);
        isodCourseMock1.addExam("kolokwium 1");
        isodCourseMock1.addExam("kolokwium 2");
        isodCourseMock1.addExam("poprawa");
        isodCourseMocks.add(isodCourseMock1);


        Set<Student> studentList2 = new HashSet<Student>() {{
            add(studentService.getStudentByLogin("kgadomski"));
            add(studentService.getStudentByLogin("anowacka"));
            add(studentService.getStudentByLogin("krychter"));
            add(studentService.getStudentByLogin("pszumanski"));
            add(studentService.getStudentByLogin("jbanachowicz"));
            add(studentService.getStudentByLogin("azegarska"));
            add(studentService.getStudentByLogin("ktarasiuk"));
        }};

        Proffesor proffesor2 = new Proffesor("Żaneta", "Świderska-Chadaj", "zanetaschadaj@example.org", "zschadaj");


        IsodCourseMock isodCourseMock2 = new IsodCourseMock(
                studentList2,
                "1DI2202:A",
                "Sztuczne sieci neuronowe - laboratorium, Laboratoria",
                "2016Z",
                proffesor2);
        isodCourseMock2.addExam("Sprawdzian 1");
        isodCourseMock2.addExam("Sprawdzian 2");
        isodCourseMock2.addExam("Sprawdzian 3");
        isodCourseMocks.add(isodCourseMock2);
    }

    public List<IsodCourseMock> getIsodCourseMocks() {
        return isodCourseMocks;
    }

}
