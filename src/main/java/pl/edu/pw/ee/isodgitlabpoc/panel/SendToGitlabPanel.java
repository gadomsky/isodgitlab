package pl.edu.pw.ee.isodgitlabpoc.panel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import pl.edu.pw.ee.isodgitlabpoc.gitlab.*;
import pl.edu.pw.ee.isodgitlabpoc.isod.Proffesor;
import pl.edu.pw.ee.isodgitlabpoc.panel.dnd.ContainerStudentsAndGitlabPanel;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class SendToGitlabPanel extends Panel implements Serializable {

    private static final long serialVersionUID = 1L;
    final Logger log = LogManager.getLogger(this.getClass());

    public SendToGitlabPanel(String id, ContainerStudentsAndGitlabPanel studentsAndGitlab, Proffesor proffesor) {
        super(id);

        final CheckBox chProffesor = new CheckBox("checkboxProffesor", Model.of(Boolean.TRUE));
        final CheckBox chSingleRepos = new CheckBox("checkboxSingleRepos", Model.of(Boolean.FALSE));

        Form<?> form = new Form<Void>("sendToGitlabForm") {
            @Override
            protected void onSubmit() {
                Boolean createSignleRepos = chSingleRepos.getModelObject();
                Boolean addProffesor = chProffesor.getModelObject();

                Map<GitlabRepo, StudentsList> mapRepoToMembers = studentsAndGitlab.getRepopanel().getMapRepoToMembers();

                //dodaj samodzielne repozytoria do mapy  // gdy ktos w panelu nieprzensoil tylko zaznaczyl checkbox
                if (createSignleRepos) {
                    studentsAndGitlab.getStudentsFromIsod().getStudentsList().stream()
                            .forEach(s -> mapRepoToMembers.put(
                                    new GitlabRepo(null, s.getLogin(), "Repozytorium studenta " + s.getName() + " " + s.getLastName()),
                                    new StudentsList(Arrays.asList(s))
                            ));
                }

                // usun grupe jesli nie potrzebna i koniec - wyjdz z metody
                if (mapRepoToMembers.keySet().size() == 0) {
                    deleteGroupIfExists(studentsAndGitlab);
                    return;
                }

                if (addProffesor) {
                    createIfNeededAndAddProffesorToGroup(proffesor, studentsAndGitlab);
                }

                try {
                    sync(studentsAndGitlab, mapRepoToMembers);
                } catch (GitlabApiException e) {
                    e.printStackTrace();
                    /////////nie udalo sie cos !!!!
                    return;
                }
            }
        };

        form.add(chProffesor);
        form.add(chSingleRepos);
        add(form);
    }

    private void sync(ContainerStudentsAndGitlabPanel studentsAndGitlab, Map<GitlabRepo, StudentsList> repoToMembersFromPanel) throws GitlabApiException {
        long gitlabGroupId = createGroupIfDoesntExistAndGetId(studentsAndGitlab);
        String gitlabGroupName = studentsAndGitlab.getGitlabGroupName();

        List<GitlabRepo> currentReposInGitlab = getCurrentGitlabReposForGroup(gitlabGroupName);

        createUnexistingReposForGroup(repoToMembersFromPanel, gitlabGroupId);
        removeUnnecessaryReposForGroup(currentReposInGitlab, repoToMembersFromPanel);

        currentReposInGitlab = getCurrentGitlabReposForGroup(gitlabGroupName);

        Map<String, User> allGitlabUsers = GitlabHelper.getInstance().getGitlabUsersManager().getAllUsers();

        for (GitlabRepo repo : currentReposInGitlab) {

            GitlabRepo repoFromPanel = repoToMembersFromPanel.keySet().stream()
                    .filter(r -> repo.getId().equals(r.getId())).findAny().get();

            StudentsList requiredStudents = repoToMembersFromPanel.get(repoFromPanel);

            Set<String> requiredStudentsLogins = requiredStudents.stream()
                    .map(s -> s.getLogin())
                    .collect(Collectors.toSet());

            List<Long> currentMemberInRepoIds = GitlabHelper.getInstance().getGitlabProjectsManager().getMembersIdsForRepo(repo);
            List<Long> requiredUsersIds = allGitlabUsers.entrySet()
                    .stream()
                    .filter(e -> requiredStudentsLogins.contains(e.getKey()))
                    .map(e -> e.getValue().getId())
                    .collect(Collectors.toList());

            addRequiredUsersToRepo(repo, currentMemberInRepoIds, requiredUsersIds);
            removeUnnecessaryUsersFromRepo(repo, currentMemberInRepoIds, requiredUsersIds);

            String newName = requiredStudents.stream()
                    .map(st -> st.getLogin())
                    .sorted()
                    .collect(Collectors.joining("_"));
            GitlabHelper.getInstance().getGitlabProjectsManager().updateRepoName(repo, newName, repoFromPanel.getDescription());
        }
    }

    private void addRequiredUsersToRepo(GitlabRepo repo, List<Long> currentMemberInRepoIds, List<Long> requiredUsersIds) throws GitlabApiException {
        Set<Long> idsOfUsersToAdd = requiredUsersIds.stream()
                .filter(s -> !currentMemberInRepoIds.contains(s))
                .collect(Collectors.toSet());

        for (Long userId : idsOfUsersToAdd) {
            GitlabHelper.getInstance().getGitlabProjectsManager().addUserToRepo(userId, repo.getId());
        }
    }

    private void removeUnnecessaryUsersFromRepo(GitlabRepo repo, List<Long> currentMemberInRepoIds, List<Long> requiredUsersIds) throws GitlabApiException {
        Set<Long> idsOfUsersToRemove = currentMemberInRepoIds.stream()
                .filter(s -> !requiredUsersIds.contains(s))
                .collect(Collectors.toSet());

        for (Long userId : idsOfUsersToRemove) {
            GitlabHelper.getInstance().getGitlabProjectsManager().removeUserFromRepo(userId, repo.getId());
        }
    }

    private void removeUnnecessaryReposForGroup(List<GitlabRepo> currentReposInGitlab, Map<GitlabRepo, StudentsList> repoToMembersFromPanel) throws GitlabApiException {
        Set<Long> requiredReposIds = repoToMembersFromPanel.keySet().stream()
                .filter(repo -> repo.getId() != null)
                .map(repo -> repo.getId())
                .collect(Collectors.toSet());

        List<GitlabRepo> reposToRemove = currentReposInGitlab.stream()
                .filter(curRepo -> !requiredReposIds.contains(curRepo.getId()))
                .collect(Collectors.toList());
        for (GitlabRepo repo : reposToRemove) {
            GitlabHelper.getInstance().getGitlabProjectsManager().removeRepo(repo.getId());
        }
    }

    private void createUnexistingReposForGroup(Map<GitlabRepo, StudentsList> repoToMembersFromPanel, long gitlabGroupId) throws GitlabApiException {
        List<GitlabRepo> reposToCreate = repoToMembersFromPanel.keySet()
                .stream()
                .filter(repo -> repo.getId() == null)
                .collect(Collectors.toList());

        for (GitlabRepo repo : reposToCreate) {
            String description = repo.getDescription();
            if (description == null || description.equals("")) {
                description = "Repozytorium dla: " + repoToMembersFromPanel.get(repo).stream().map(st -> st.getName() + " " + st.getLastName()).collect(Collectors.joining(", "));
            }
            long repoId = GitlabHelper.getInstance().getGitlabProjectsManager().createProjectForGroup(gitlabGroupId, repo.getName(), description);
            repo.setId(repoId);
        }
    }

    private List<GitlabRepo> getCurrentGitlabReposForGroup(String groupName) throws GitlabApiException {
        return GitlabHelper.getInstance().getGitlabProjectsManager().getReposForGroup(groupName);
    }

    private long createGroupIfDoesntExistAndGetId(ContainerStudentsAndGitlabPanel studentsAndGitlab) throws GitlabApiException {
        return GitlabHelper.getInstance().getGitlabGroupsManager().createGroupIfDoesntExists(studentsAndGitlab.getGitlabGroupName(), studentsAndGitlab.getGitlabGroupDescription());
    }

    private void deleteGroupIfExists(ContainerStudentsAndGitlabPanel studentsAndGitlab) {
        GitlabGroupsManager gitlabGroupsManager = GitlabHelper.getInstance().getGitlabGroupsManager();

        try {
            gitlabGroupsManager.deleteGitlabGroupIfExists(studentsAndGitlab.getGitlabGroupName());
        } catch (GitlabApiException e) {
            e.printStackTrace();
        }
    }

    private void createIfNeededAndAddProffesorToGroup(Proffesor proffesor, ContainerStudentsAndGitlabPanel studentsAndGitlab) {
        try {
            Long proffeseorId = GitlabHelper.getInstance().getGitlabUsersManager().getUserId(proffesor.getLogin());
            if (proffeseorId == null) {
                proffeseorId = GitlabHelper.getInstance().getGitlabUsersManager().createUser(Util.proffesorToJson(proffesor)).getId();
            }
            long gitlabGroupId = createGroupIfDoesntExistAndGetId(studentsAndGitlab);
            GitlabHelper.getInstance().getGitlabGroupsManager().addUserToGroup(proffeseorId, gitlabGroupId, true);
        } catch (GitlabApiException e) {
            this.log.debug("Trying to add again user to grup - nothing happend");
        }
    }


}
