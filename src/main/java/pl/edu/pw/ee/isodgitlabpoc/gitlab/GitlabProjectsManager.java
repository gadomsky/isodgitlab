package pl.edu.pw.ee.isodgitlabpoc.gitlab;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Krzysiek on 10.01.2017.
 */
public class GitlabProjectsManager {

    final Logger log = LogManager.getLogger(this.getClass());

    GitlabApiConnection gitlabApiConnection;

    GitlabProjectsManager(GitlabApiConnection gitlabApiConnection) {
        this.gitlabApiConnection = gitlabApiConnection;
    }

    public List<GitlabRepo> getReposForGroup(String groupName) throws GitlabApiException {
        return (List<GitlabRepo>) this.gitlabApiConnection.getRequestWithPagination("projects/all")
                .stream()
                .filter(o -> ((JSONObject) o).get("owner") == null) //tylko projekty bedace w grupach, a nie nalezace do uzytkownikow
                .filter(o -> ((JSONObject) ((JSONObject) o).get("namespace")).get("name").equals(groupName))
                .map(o -> new GitlabRepo((long) ((JSONObject) o).get("id"), (String) ((JSONObject) o).get("name"), (String)  ((JSONObject) o).get("description")))
                .collect(Collectors.toList());
    }

    public List<String> getMembersForRepo(GitlabRepo repo) throws GitlabApiException {
        return (List<String>) this.gitlabApiConnection.getRequestWithPagination("projects/" + repo.getId() + "/members")
                .stream()
                .map(o -> ((JSONObject) o).get("username"))
                .collect(Collectors.toList());
    }

    public List<Long> getMembersIdsForRepo(GitlabRepo repo) throws GitlabApiException {
        return (List<Long>) this.gitlabApiConnection.getRequestWithPagination("projects/" + repo.getId() + "/members")
                .stream()
                .map(o -> ((JSONObject) o).get("id"))
                .collect(Collectors.toList());
    }


    public long createProjectForGroup(long groupId, String repoName, String description) throws GitlabApiException {
        JSONObject response = this.gitlabApiConnection.postRequest("projects", Util.groupAndRepoToJson(groupId, repoName, description));
        return (long) response.get("id");
    }

    public void removeUserFromRepo(Long userId, Long repoId) throws GitlabApiException {
        log.info("Removing user[id=" + userId + "] from repo[id=" + repoId + "]");
        this.gitlabApiConnection.deleteRequest("/projects/"+repoId+"/members/"+userId);
    }

    public void addUserToRepo(Long userId, Long repoId) throws GitlabApiException {
        log.info("Adding user[id=" + userId + "] to gitlab repo[id=" + repoId + "]");
        Map<String, String> jsonMap = new HashMap<>();
        jsonMap.put("user_id", "" + userId);
        jsonMap.put("access_level", "30");
        JSONObject json = new JSONObject(jsonMap);

        this.gitlabApiConnection.postRequest("projects/" + repoId + "/members", json.toJSONString());
    }

    public void removeRepo(long repoId) throws GitlabApiException {
        this.gitlabApiConnection.deleteRequestBooleanResponse("projects/"+repoId);
    }

    public void updateRepoName(GitlabRepo repo, String newName,String description) throws GitlabApiException {
        this.gitlabApiConnection.putRequest("/projects/" + repo.getId(), Util.repoToJson(repo.getId(),newName, description));
    }
}
