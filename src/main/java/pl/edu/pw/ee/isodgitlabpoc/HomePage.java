package pl.edu.pw.ee.isodgitlabpoc;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import pl.edu.pw.ee.isodgitlabpoc.isod.IsodCourseMock;

public class HomePage extends WebPage {
    private static final long serialVersionUID = 1L;

    public HomePage(final PageParameters parameters) {
        super(parameters);

        add(new Label("version", getApplication().getFrameworkSettings().getVersion()));

    }

    public HomePage(IsodCourseMock isodCourseMock1) {
        final StringBuilder st = new StringBuilder();
        isodCourseMock1.getStudents().forEach(s -> {
            st.append(s.getEmail());
            st.append("\\n");

        });
        add(new Label("version","Studenci: " + st));
    }
}
