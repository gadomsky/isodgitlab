package pl.edu.pw.ee.isodgitlabpoc.isod;

import java.io.Serializable;

/**
 * Created by Krzysiek on 14.01.2017.
 */
public class ExamWithRepo implements Serializable{
    private static final long serialVersionUID = 1L;
    private final String title;
    private final IsodCourseMock isodCourse;

    public ExamWithRepo(String title, IsodCourseMock isodCourse) {
        this.title = title;
        this.isodCourse = isodCourse;
    }

    public String getTitle() {
        return title;
    }

    public IsodCourseMock getIsodCourse() {
        return isodCourse;
    }

    public String getGitlabGroupName() {
        return (isodCourse.getSemesterSymbol() + "_" + isodCourse.getSubjectSymbol() + "_" + title).replaceAll(" ", "_").replaceAll("/", "_").replaceAll(":","_");
    }
}
