package pl.edu.pw.ee.isodgitlabpoc.panel.dnd;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import pl.edu.pw.ee.isodgitlabpoc.gitlab.GitlabHelper;
import pl.edu.pw.ee.isodgitlabpoc.isod.Student;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by Krzysiek on 20.01.2017.
 */
public class RepoPanel extends Panel {
    private Long gitlabId;
    private String description;
    private String gitlabGroup;

    private StudentsDnDComponent studentsDnDComponent;

    public StudentsDnDComponent getStudentsDnDComponent() {
        return studentsDnDComponent;
    }

    public RepoPanel(String id, Collection<Student> students) {
        this(id, null, null, null, students);
    }

    public RepoPanel(String id, Long gitlabId, String gitlabGroup, String desc, Collection<Student> students) {
        super(id);
        this.gitlabId = gitlabId;
        this.gitlabGroup = gitlabGroup;
        this.description = desc;
        WebMarkupContainer content = new WebMarkupContainer("webmc");
        content.setOutputMarkupId(true);
        add(content);

        studentsDnDComponent = new StudentsDnDComponent("vertical", students, "(przeciągnij studentów)");
        studentsDnDComponent.setOutputMarkupId(true);

        content.add(studentsDnDComponent);


        ExternalLink link = new ExternalLink("gitlabLink", GitlabHelper.getInstance().getGitlabUrl() + "/" + gitlabGroup + "/" + getRepoName() ) ;
        link.setVisible(isRepoCreated());
        add(link);

        TextField<String> descriptionField = new TextField<>("descriptionField", Model.of(""));
        descriptionField.add(new OnChangeAjaxBehavior() {
            private static final long serialVersionUID = 2462233190993745889L;

            @Override
            protected void onUpdate(final AjaxRequestTarget target) {
                description = ((TextField<String>) getComponent()).getModelObject();
            }
        });
        descriptionField.setModelValue(new String[]{desc});
        descriptionField.setOutputMarkupId(true);
        add(descriptionField);

    }

    public boolean isRepoCreated() {
        if (gitlabId == null) return false;
        else return true;
    }

    public String getDescription() {
        return description;
    }

    public Long getGitlabId() {
        return gitlabId;
    }

    public String getRepoName() {
        return studentsDnDComponent.getStudentsList().stream()
                .map(st -> st.getLogin())
                .sorted()
                .collect(Collectors.joining("_"));
    }
}
