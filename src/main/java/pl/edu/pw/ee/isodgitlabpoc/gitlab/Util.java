package pl.edu.pw.ee.isodgitlabpoc.gitlab;

import org.json.simple.JSONObject;
import pl.edu.pw.ee.isodgitlabpoc.isod.Proffesor;
import pl.edu.pw.ee.isodgitlabpoc.isod.Student;

import java.util.HashMap;
import java.util.Map;

public class Util {
    static final String chars="qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890-+=,.;_ ";

    public static String studentToJson(Student st) {

        HashMap<String, String> map = new HashMap<>();
        map.put("username", st.getLogin());
        map.put("email", st.getEmail());
        map.put("name", st.getName() + " " + st.getLastName());
        map.put("password", generateRandomPassword(10));

        return JSONObject.toJSONString(map);
    }

    public static String groupAndRepoToJson(long groupNameSpaceId, String repoName, String description){
        Map<String,String> map = new HashMap<>();
        map.put("name", repoName);
        map.put("namespace_id", String.valueOf(groupNameSpaceId));
        map.put("description", description);

        return JSONObject.toJSONString(map);
    }

    public static String repoToJson(long repoId, String nameAndPath, String description) {
        Map<String,String> map = new HashMap<>();
        map.put("id", String.valueOf(repoId));
        map.put("name", nameAndPath);
        map.put("path", nameAndPath);
        map.put("description", description);

        return JSONObject.toJSONString(map);
    }

    public static String generateRandomPassword(int len){
        String ret="";
        for (int i=0;i<len;i++){
            int nr=(int)(Math.random()*(chars.length()-1));
            ret+=chars.charAt(nr);
        }
        return ret;
    }

    public static String proffesorToJson(Proffesor proffesor) {
        HashMap<String, String> map = new HashMap<>();
        map.put("username", proffesor.getLogin());
        map.put("email", proffesor.getEmail());
        map.put("name", proffesor.getName() + " " + proffesor.getLastName());
        map.put("password", generateRandomPassword(10));

        return JSONObject.toJSONString(map);
    }
}

