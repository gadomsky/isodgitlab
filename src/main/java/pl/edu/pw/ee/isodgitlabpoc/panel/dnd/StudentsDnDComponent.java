package pl.edu.pw.ee.isodgitlabpoc.panel.dnd;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.IAjaxIndicatorAware;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.resource.ContextRelativeResource;
import pl.edu.pw.ee.isodgitlabpoc.gitlab.GitlabApiException;
import pl.edu.pw.ee.isodgitlabpoc.gitlab.GitlabHelper;
import pl.edu.pw.ee.isodgitlabpoc.isod.Student;
import pl.edu.pw.ee.isodgitlabpoc.panel.StudentsList;
import wicketdnd.*;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

/**
 * Created by kgadomski on 19.01.17.
 */
public class StudentsDnDComponent extends WebMarkupContainer implements Serializable, IAjaxIndicatorAware {
    private final Logger log = LogManager.getLogger(this.getClass());

    private String[] types = new String[]{Transfer.ANY};

    final StudentsList studentsList;

    public String[] types() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }

    protected Student operate(Transfer transfer) {
        Student foo = transfer.getData();
        switch (transfer.getOperation()) {
            case MOVE:
            case COPY:
                return foo;
            case LINK:
                return foo;
            default:
                throw new IllegalArgumentException();
        }
    }

    public StudentsDnDComponent(String id, Collection<Student> students, String label) {
        super(id);
        Label infoLabel = new Label("info", label);
        infoLabel.setOutputMarkupId(true);
        add(infoLabel);

        if (students == null) {
            students = Collections.emptyList();
        }
        studentsList = new StudentsList(students);

        final ListView<Student> items = getStudentsListView(studentsList);
        this.add(items);
        DragSource dragSource = getDragSource(studentsList, this);
        DropTarget dropTarget = getDropTarget(studentsList, this);
        this.add(dragSource);

        if ("vertical".equals(id)) {
            dropTarget.dropTopAndBottom("div.item");
        } else {
            dropTarget.dropLeftAndRight("div.item");
        }
        this.add(dropTarget);
    }


    private ListView<Student> getStudentsListView(final StudentsList items) {
        return new ListView<Student>("items", items) {
            @Override
            protected ListItem<Student> newItem(int index, IModel<Student> model) {
                ListItem<Student> item = super.newItem(index, model);
                item.setOutputMarkupId(true);
                return item;
            }

            @Override
            protected void populateItem(ListItem<Student> item) {
                Student st = item.getModel().getObject();

                item.add(new Label("name", st.getName() + " " + st.getLastName() + " (" + st.getLogin() + ")"));

                Image isActiveImage = new Image("isActiveImage", new ContextRelativeResource("/inactive.png"));
                boolean userActive = false;
                try {
                    userActive = GitlabHelper.getInstance().getGitlabUsersManager().isUserActive(st.getLogin());
                } catch (GitlabApiException e) {
                    log.info("Error while getting user info", e);
                }
                isActiveImage.setVisible(!userActive);
                item.add(isActiveImage);
            }
        };
    }

    DragSource getDragSource(StudentsList studentsList, WebMarkupContainer dndListComponent) {
        return new DragSource() {
            @Override
            public Set<Operation> getOperations() {
                return EnumSet.of(Operation.MOVE);
                //return dragOperations();
            }

            @Override
            public String[] getTypes() {
                return types();
            }

            @Override
            public void onAfterDrop(AjaxRequestTarget target, Transfer transfer) {
                if (transfer.getOperation() == Operation.MOVE) {
                    studentsList.remove(transfer.getData());
                    target.add(dndListComponent);
                }
            }
        }.drag("div.item").initiate("span.initiate");
    }

    DropTarget getDropTarget(StudentsList studentsList, WebMarkupContainer dndListComponent) {
        return new DropTarget() {
            @Override
            public Set<Operation> getOperations() {
                return EnumSet.of(Operation.MOVE);
                //return dropOperations();
            }

            @Override
            public String[] getTypes() {
                return types();
            }

            @Override
            public void onDrop(AjaxRequestTarget target, Transfer transfer, Location location) {
                if (location.getComponent() == dndListComponent) {
                    Student st = operate(transfer);
                    studentsList.add(st);
                    target.add(dndListComponent);
                } else {
                    if (location.getModelObject().getClass().equals(String.class)) {
                        studentsList.add(operate(transfer));
                        target.add(dndListComponent);
                        return;
                    }
                    Student student = location.getModelObject();
                    switch (location.getAnchor()) {
                        case TOP:
                        case LEFT:
                            studentsList.addBefore(operate(transfer), student);
                            break;
                        case BOTTOM:
                        case RIGHT:
                            studentsList.addAfter(operate(transfer), student);
                            break;
                        default:
                            transfer.reject();
                    }

                    target.add(dndListComponent);
                }
            }
        };
    }


    @Override
    public String getAjaxIndicatorMarkupId() {
        return "veil";
    }

 /*   public StudentsList getInfo() {
        return studentsList;
    }*/

    public StudentsList getStudentsList() {
        return studentsList;
    }
}
