package pl.edu.pw.ee.isodgitlabpoc.isod;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Krzysiek on 27.12.2016.
 */
public final class StudentService implements Serializable {
    private static final long serialVersionUID = 1L;

    private static StudentService INSTANCE;

    private Map<String, Student> login2student = new HashMap<>();

    private StudentService() {
        //ponieważ jest to tylko POC i nie ma docelowej integracji z ISODEM, tworzę tutaj fikcyjnych użytkowników;
        login2student.put("kgadomski", new Student("Krzysztof", "Gadomski", "kgadomski@example.org", "kgadomski"));
        login2student.put("itunel", new Student("Ireneusz", "Tunel", "itunel@example.org", "itunel"));
        login2student.put("jnowak", new Student("Jan", "Nowak", "jnowak@example.org", "jnowak"));
        login2student.put("probak", new Student("Patryk", "Robak", "probak@example.org", "probak"));
        login2student.put("kmarek", new Student("Kamil", "Marek", "kmarek@example.org", "kmarek"));
        login2student.put("anowacka", new Student("Anna", "Nowkacka", "anowacka@example.org", "anowacka"));
        login2student.put("krychter", new Student("Karol", "Rychter", "krychter@example.org", "krychter"));
        login2student.put("pszumanski", new Student("Paweł", "Szumanski", "pszumanski@example.org", "pszumanski"));
        login2student.put("jbanachowicz", new Student("Joanna", "Banachowicz", "jbanachowicz@example.org", "jbanachowicz"));
        login2student.put("azegarska", new Student("Anita", "Zegarska", "azegarska@example.org", "azegarska"));
        login2student.put("ktarasiuk", new Student("Katarzyna", "Tarasiuk", "ktarasiuk@example.org", "ktarasiuk"));
    }

    public static StudentService getInstance() {
        if (INSTANCE == null) INSTANCE = new StudentService();
        return INSTANCE;
    }

    public Collection<Student> getAllStudents() {
        return login2student.values();
    }

    public Student getStudentByLogin(String login) {
        return login2student.get(login);
    }

    public Set<Student> getStudentsByLogins(List<String> logins) {
        return logins.stream()
                .map( l -> getStudentByLogin(l))
                .collect(Collectors.toSet()) ;
    }

}
