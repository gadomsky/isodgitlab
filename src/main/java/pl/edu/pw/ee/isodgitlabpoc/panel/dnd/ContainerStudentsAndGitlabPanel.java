package pl.edu.pw.ee.isodgitlabpoc.panel.dnd;

import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.IAjaxIndicatorAware;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import pl.edu.pw.ee.isodgitlabpoc.gitlab.GitlabApiException;
import pl.edu.pw.ee.isodgitlabpoc.gitlab.GitlabHelper;
import pl.edu.pw.ee.isodgitlabpoc.gitlab.Util;
import pl.edu.pw.ee.isodgitlabpoc.isod.ExamWithRepo;
import pl.edu.pw.ee.isodgitlabpoc.isod.Student;
import wicketdnd.theme.WebTheme;

import java.io.Serializable;
import java.util.Collection;

public class ContainerStudentsAndGitlabPanel extends Panel implements IAjaxIndicatorAware, Serializable {

    private static final long serialVersionUID = 1L;
    private String title = "Zarządzaj repozytoriami w Gitlabie";

    private GitlabPanel repopanel;
    private StudentsDnDComponent studentsFromIsod;

    private WebMarkupContainer content;
    private Collection<Student> students;
    private String gitlabGroupName;
    private String gitlabGroupDescription;

    public ContainerStudentsAndGitlabPanel(String id, ExamWithRepo examWithRepo) {
        super(id);

        this.students = examWithRepo.getIsodCourse().getStudents();
        this.gitlabGroupName = examWithRepo.getGitlabGroupName();
        this.gitlabGroupDescription = examWithRepo.getIsodCourse().getTitle();

        content = new WebMarkupContainer("content");
        content.setOutputMarkupId(true);

        Button initAccountsButton = new Button("initAccountsButton");
        initAccountsButton.add(new AjaxEventBehavior("click") {
            @Override
            protected void onEvent(AjaxRequestTarget target) {
                students
                        .stream()
                        .filter(s -> {
                            try {
                                return !GitlabHelper.getInstance().getGitlabUsersManager().isUserActive(s.getLogin());
                            } catch (GitlabApiException e) {
                                e.printStackTrace();
                                return true;
                            }
                        })
                        .forEach(s -> {
                            try {
                                GitlabHelper.getInstance().getGitlabUsersManager().createUser(Util.studentToJson(s));
                            } catch (GitlabApiException e) {
                                e.printStackTrace();
                            }
                        });
                target.add(content);
            }
        });
        add(initAccountsButton);

        add(content);

        add(new Label("title", (IModel<String>) () -> title));


        //STUDENCI do wyboru
/*
        List<Student> studentList = new ArrayList<>(students);
        List<Student> toRemove = repopanel.getMapRepoToMembers().values()
                .stream()
                .flatMap(col -> col.stream()).distinct()
                .collect(Collectors.toList());
        studentList.removeAll(toRemove);
*/

        studentsFromIsod = new StudentsDnDComponent("vertical", students, "Studenci:");
        //GitlabPanel
        repopanel = new GitlabPanel("repopanel", examWithRepo, studentsFromIsod);
        content.add(repopanel);
        content.add(studentsFromIsod);

        content.add(new WebTheme());
    }

    @Override
    public String getAjaxIndicatorMarkupId() {
        return "veil";
    }

    public GitlabPanel getRepopanel() {
        return repopanel;
    }

    public StudentsDnDComponent getStudentsFromIsod() {
        return studentsFromIsod;
    }

    public String getGitlabGroupName() {
        return gitlabGroupName;
    }

    public String getGitlabGroupDescription() {
        return gitlabGroupDescription;
    }
}
