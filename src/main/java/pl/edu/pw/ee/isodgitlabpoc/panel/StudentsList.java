package pl.edu.pw.ee.isodgitlabpoc.panel;

import pl.edu.pw.ee.isodgitlabpoc.isod.Student;

import java.io.Serializable;
import java.util.*;

public class StudentsList extends LinkedList<Student> implements Serializable {
    private static final long serialVersionUID = 1L;

    public StudentsList(Collection<Student> allStudents) {
        super(allStudents);
    }

    public void addBefore(Student drag, Student drop) {

        add(indexOf(drop), drag);
    }

    public void addAfter(Student drag, Student drop) {
        //drag.remove();
        add(indexOf(drop) + 1, drag);
    }
}
