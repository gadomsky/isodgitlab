package pl.edu.pw.ee.isodgitlabpoc.panel.dnd;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;
import pl.edu.pw.ee.isodgitlabpoc.gitlab.GitlabApiException;
import pl.edu.pw.ee.isodgitlabpoc.gitlab.GitlabHelper;
import pl.edu.pw.ee.isodgitlabpoc.gitlab.GitlabRepo;
import pl.edu.pw.ee.isodgitlabpoc.isod.ExamWithRepo;
import pl.edu.pw.ee.isodgitlabpoc.isod.Student;
import pl.edu.pw.ee.isodgitlabpoc.isod.StudentService;
import pl.edu.pw.ee.isodgitlabpoc.panel.StudentsList;

import java.io.Serializable;
import java.util.*;

/**
 * Created by Krzysiek on 20.01.2017.
 */
public class GitlabPanel extends Panel implements Serializable {

    private final Logger log = LogManager.getLogger(this.getClass());
    List<RepoPanel> studentsComponents = new LinkedList<>();
    private ListView<RepoPanel> list;

    public GitlabPanel(String id, ExamWithRepo examWithRepo, StudentsDnDComponent studentsFromIsod) {
        super(id);

        list = new ListView<RepoPanel>("repoPanelsListView",
                new PropertyModel<>(this, "studentsComponents")) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void populateItem(ListItem<RepoPanel> item) {
                item.add(item.getModelObject());
                item.add(list.removeLink("rem", item).add(new AjaxEventBehavior("click") {
                    @Override
                    protected void onEvent(AjaxRequestTarget target) {
                        StudentsList removedStudents = item.getModelObject().getStudentsDnDComponent().getStudentsList();
                        studentsFromIsod.getStudentsList().addAll(removedStudents);
                        target.add(studentsFromIsod);
                    }
                }));
            }
        };

        list.setOutputMarkupId(true);


        WebMarkupContainer wmc = new WebMarkupContainer("wmc");
        add(wmc);
        wmc.setOutputMarkupId(true);
        wmc.add(new AjaxLink<String>("addPanelLink") {
            private static final long serialVersionUID = 1L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                try {
                    RepoPanel newStudentsComponent = new RepoPanel("repoPanel", Collections.emptyList());
                    newStudentsComponent.setOutputMarkupId(true);
                    list.getModelObject().add(newStudentsComponent);
                    target.add(wmc);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        List<GitlabRepo> reposForGroup;
        try {
            reposForGroup = GitlabHelper.getInstance().getGitlabProjectsManager().getReposForGroup(examWithRepo.getGitlabGroupName());
        } catch (GitlabApiException e) {
            log.info("Error while getting projects for group, returned empty list", e);
            reposForGroup = Collections.emptyList();
        }

        for (GitlabRepo gitlabRepo : reposForGroup) {
            Set<Student> students;
            try {
                List<String> userNamesForRepo = GitlabHelper.getInstance().getGitlabProjectsManager().getMembersForRepo(gitlabRepo);
                students = StudentService.getInstance().getStudentsByLogins(userNamesForRepo);
            } catch (GitlabApiException e) {
                log.info("Error while getting users for repo, returned empty list", e);
                students = Collections.emptySet();
            }
            RepoPanel repoPanel = new RepoPanel("repoPanel", gitlabRepo.getId(), examWithRepo.getGitlabGroupName(), gitlabRepo.getDescription(), students);
            studentsFromIsod.getStudentsList().removeAll(students);
            studentsComponents.add(repoPanel);
        }


        wmc.add(list);
    }

    public Map<GitlabRepo, StudentsList> getMapRepoToMembers() {
        Map<GitlabRepo, StudentsList> ret = new HashMap<>();
        this.studentsComponents
                .stream()
                .forEach(repoPanel -> {
                    ret.put(new GitlabRepo(repoPanel.getGitlabId(), repoPanel.getRepoName(), repoPanel.getDescription()), repoPanel.getStudentsDnDComponent().getStudentsList());
                });
        return ret;
    }

    public List<RepoPanel> getStudentsComponents() {
        return studentsComponents;
    }
}
