package pl.edu.pw.ee.isodgitlabpoc.gitlab;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.json.simple.JSONObject;

public class GitlabHelper {

    private static final class Holder{
        static final GitlabHelper INSTANCE = new GitlabHelper();
    }

    final Logger log = LogManager.getLogger(this.getClass());

    private final GitlabApiConnection gitlabApiConnection = new GitlabApiConnection();

    private final GitlabUsersManager gitlabUsersManager = new GitlabUsersManager(gitlabApiConnection);
    private final GitlabProjectsManager gitlabProjectsManager = new GitlabProjectsManager(gitlabApiConnection);
    private final GitlabGroupsManager gitlabGroupsManager = new GitlabGroupsManager(gitlabApiConnection);

    private GitlabHelper() {
    }

    public static GitlabHelper getInstance() {
        return Holder.INSTANCE;
    }

    public GitlabProjectsManager getGitlabProjectsManager() {
        return gitlabProjectsManager;
    }

    public GitlabUsersManager getGitlabUsersManager() {
        return gitlabUsersManager;
    }

    public GitlabGroupsManager getGitlabGroupsManager() {
        return gitlabGroupsManager;
    }

    public boolean amIAdmin() throws GitlabApiException {
        log.info("Checking if am I admin");
        return (boolean) this.gitlabApiConnection.getRequest("user").get("is_admin");
    }

    public JSONObject getApplicationSettings() throws GitlabApiException {
        log.info("Getting application settings");
        return this.gitlabApiConnection.getRequest("application/settings");
    }

    public void updateApplicationSettings(String settingsJson) throws GitlabApiException {
        log.info("Updating application settings");
        this.gitlabApiConnection.putRequest("application/settings", settingsJson);
    }

    public String getGitlabUrl() {
        return this.gitlabApiConnection.getGitlabUrl();
    }
}
