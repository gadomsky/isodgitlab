package pl.edu.pw.ee.isodgitlabpoc.panel;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import pl.edu.pw.ee.isodgitlabpoc.isod.ExamWithRepo;
import pl.edu.pw.ee.isodgitlabpoc.isod.IsodCourseMock;
import pl.edu.pw.ee.isodgitlabpoc.isod.IsodMockService;
import pl.edu.pw.ee.isodgitlabpoc.panel.dnd.ContainerStudentsAndGitlabPanel;
import wicketdnd.IEBackgroundImageCacheFix;
import wicketdnd.IECursorFix;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class IsodGitlabModule extends WebPage implements Serializable {
    private static final long serialVersionUID = 1L;

    private String selectedCourse;
    private String selectedExam;
    Panel studentsListComponent;
    Panel sendToGtilabComponent;

    public void setSelectedCourse(String selectedCourse) {
        this.selectedCourse = selectedCourse;
    }

    public IsodGitlabModule() {
        Map<String, IsodCourseMock> isodCourseMockMap = IsodMockService.getInstance().getIsodCourseMocks()
                .stream()
                .collect(Collectors.toMap(s -> s.getSemesterSymbol() + "/" + s.getTitle(), s -> s));

        IModel<List<ExamWithRepo>> examChoices = (IModel<List<ExamWithRepo>>) () -> {
            List<ExamWithRepo> exams = Collections.emptyList();
            if (isodCourseMockMap.get(selectedCourse) != null) {
                exams = isodCourseMockMap.get(selectedCourse).getExams()
                        .stream()
                        .collect(Collectors.toList());
            }
            return exams;
        };

        Form<?> form = new Form("form");
        add(form);

        final DropDownChoice<String> courses = new DropDownChoice<>("courses",
                new PropertyModel<String>(this, "selectedCourse"),
                (IModel<List<? extends String>>) () -> new ArrayList<>(isodCourseMockMap.keySet()));


        final DropDownChoice<String> exams = new DropDownChoice<>("exams",
                new PropertyModel<String>(this, "selectedExam"),
                (IModel<List<? extends String>>) () -> new ArrayList<>(examChoices.getObject().stream().map(e -> e.getTitle()).collect(Collectors.toList())));


        courses.add(new AjaxFormComponentUpdatingBehavior("change") {
            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                target.add(exams);
            }
        });

        exams.setOutputMarkupId(true);
        courses.setOutputMarkupId(true);
        form.add(courses);
        form.add(exams);

        final FeedbackPanel feedback = new FeedbackPanel("feedback");
        feedback.setOutputMarkupId(true);

        add(feedback);

        studentsListComponent = new MyEmptyPanel("studentsListComponent");
        studentsListComponent.setOutputMarkupId(true);
        add(studentsListComponent);

        sendToGtilabComponent = new MyEmptyPanel("sendToGtilabPanel");
        sendToGtilabComponent.setOutputMarkupId(true);
        add(sendToGtilabComponent);

        form.add(new AjaxButton("go") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onAfterSubmit(AjaxRequestTarget target) {
                super.onAfterSubmit(target);

                if (selectedExam!=null) {
                    ExamWithRepo exam = isodCourseMockMap.get(selectedCourse).getExams().stream().filter(e -> e.getTitle().equals(selectedExam)).findAny().get();
                    info("Prowadzący : " + exam.getIsodCourse().getProffesor().toString());
                    target.add(feedback);

                    ContainerStudentsAndGitlabPanel newContainer = new ContainerStudentsAndGitlabPanel("studentsListComponent", exam);
                    newContainer.setOutputMarkupId(true);
                    studentsListComponent.replaceWith(newContainer);
                    target.add(newContainer);
                    studentsListComponent = newContainer;


                    SendToGitlabPanel sendToGitlabPanel = new SendToGitlabPanel("sendToGtilabPanel", newContainer, exam.getIsodCourse().getProffesor());
                    sendToGitlabPanel.setOutputMarkupId(true);
                    sendToGtilabComponent.replaceWith(sendToGitlabPanel);
                    target.add(sendToGitlabPanel);
                    sendToGtilabComponent = sendToGitlabPanel;
                }
            }
        });
        add(new IECursorFix());
        add(new IEBackgroundImageCacheFix());
    }
}


