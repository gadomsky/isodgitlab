package pl.edu.pw.ee.isodgitlabpoc.gitlab;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Krzysiek on 10.01.2017.
 */
public class GitlabGroupsManager {

    final Logger log = LogManager.getLogger(this.getClass());

    GitlabApiConnection gitlabApiConnection;

    GitlabGroupsManager(GitlabApiConnection gitlabApiConnection) {
        this.gitlabApiConnection = gitlabApiConnection;
    }


    public void blockGroupCreatingForUser(User u) throws GitlabApiException {
        Map<String, String> jsonMap = new HashMap<>();
        jsonMap.put("can_create_group", "false");
        JSONObject json = new JSONObject(jsonMap);
        this.gitlabApiConnection.putRequest("users/" + u.getId(), json.toJSONString());
    }

    public Map<Long, String> getAllGroups() throws GitlabApiException {
        log.info("Getting all Gitlab groups");
        JSONArray groups = this.gitlabApiConnection.getRequestWithPagination("groups");

        Map<Long, String> idToGroupPath = new HashMap<>();
        groups.forEach(o -> idToGroupPath.put((Long) ((JSONObject) o).get("id"), (String) ((JSONObject) o).get("path")));
        return idToGroupPath;
    }

    public long createGroup(String path, String description) throws GitlabApiException {
        log.info("Creating gitlab group " + path);
        Map<String, String> jsonMap = new HashMap<>();
        jsonMap.put("name", path);
        jsonMap.put("path", path);
        jsonMap.put("description", description);
        JSONObject json = new JSONObject(jsonMap);
        JSONObject group = this.gitlabApiConnection.postRequest("groups", json.toJSONString());
        return (long) group.get("id");
    }

    public long createGroupIfDoesntExists(String path, String description) throws GitlabApiException {
        Optional<Long> groupIdIfExists = getGroupIdIfExists(path);
        return groupIdIfExists
                .orElseGet(() -> {
                    try {
                        return createGroup(path, description);
                    } catch (GitlabApiException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
                );
    }

    public Map<String, Long> getUsersForGroup(long groupId) throws GitlabApiException {
        log.info("Getting users for gitlab group [id=" + groupId + "]");
        JSONArray usersArray = this.gitlabApiConnection.getRequestWithPagination("groups/" + groupId + "/members");

        Map<String, Long> userNamesToAccessLvl = new HashMap<>();

        usersArray.stream().forEach(o -> {
            JSONObject o1 = (JSONObject) o;
            userNamesToAccessLvl.put((String) o1.get("username"), (Long) o1.get("access_level"));
        });

        return userNamesToAccessLvl;
    }

    public void addUserToGroup(long userId, long groupId, boolean owner) throws GitlabApiException {
        log.info("Adding user[id=" + userId + "] to gitlab group[id=" + groupId + "]");
        Map<String, String> jsonMap = new HashMap<>();
        jsonMap.put("user_id", "" + userId);
        jsonMap.put("access_level", owner ? "50" : "30");
        JSONObject json = new JSONObject(jsonMap);

        this.gitlabApiConnection.postRequest("groups/" + groupId + "/members", json.toJSONString());
    }

    public void deleteGitlabGroupIfExists(String gitlabGroupName) throws GitlabApiException {
        this.gitlabApiConnection.getRequestWithPaginationWithQuery("groups", "search", gitlabGroupName)
                .stream()
                .filter(o -> (((JSONObject) o).get("name").equals(gitlabGroupName)))
                .map(o -> (((JSONObject) o).get("id")))
                .findAny().ifPresent(id -> {
            try {
                this.gitlabApiConnection.deleteRequest("groups/" + id);
            } catch (GitlabApiException e) {
                e.printStackTrace();
            }
        });
    }

    private Optional<Long> getGroupIdIfExists(String gitlabGroupName) throws GitlabApiException {
        System.out.println(this.getAllGroups());
        return this.getAllGroups().entrySet()
                .stream()
                .filter(en -> en.getValue().equals(gitlabGroupName))
                .map(en -> en.getKey())
                .findFirst();
    }
}
