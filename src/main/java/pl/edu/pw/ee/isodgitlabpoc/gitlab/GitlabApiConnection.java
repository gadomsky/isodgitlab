package pl.edu.pw.ee.isodgitlabpoc.gitlab;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Krzysiek on 10.01.2017.
 */
public class GitlabApiConnection {

    private final String gitlabUrl;
    private final String gitlabToken;
    private final Logger log = LogManager.getLogger(this.getClass());
    private final Client client;

    GitlabApiConnection() {
        Properties props = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("gitlab.properties");
        try {
            props.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        gitlabUrl = (String) props.get("gitlab.url");
        gitlabToken = (String) props.get("gitlab.token");
        this.client = ClientBuilder.newClient(new ClientConfig());
    }

    boolean putRequest(String resource, String jsonBody) throws GitlabApiException {
        WebTarget webTarget = getWebTarget(resource);
        String uri = getUriString(webTarget);
        log.info("Reqesting uri: " + uri + ", method=PUT, body= " + jsonBody);

        Response response = webTarget.request(MediaType.APPLICATION_JSON_TYPE).put(Entity.entity(jsonBody, MediaType.APPLICATION_JSON_TYPE));

        int responseStatus = response.getStatus();
        if (responseStatus == 200) {
            return true;
        } else {
            String error = response.readEntity(String.class);
            log.info("Error occured: " + error);
            throw new GitlabApiException(error);
        }
    }

    JSONObject deleteRequest(String resource) throws GitlabApiException {
        WebTarget webTarget = getWebTarget(resource);
        String uri = getUriString(webTarget);
        log.info("Reqesting uri: " + uri + ", method=DELETE");

        Response response = webTarget.request(MediaType.APPLICATION_JSON_TYPE).delete();
        if (response.getStatus() == 200) {
            return (JSONObject) JSONValue.parse(response.readEntity(String.class));
        } else {
            String error = response.readEntity(String.class);
            log.info("Error occured: " + error);
            throw new GitlabApiException(error);
        }

    }

    Boolean deleteRequestBooleanResponse(String resource) throws GitlabApiException {
        WebTarget webTarget = getWebTarget(resource);
        String uri = getUriString(webTarget);
        log.info("Reqesting uri: " + uri + ", method=DELETE");

        Response response = webTarget.request(MediaType.APPLICATION_JSON_TYPE).delete();
        if (response.getStatus() == 200) {
            return (Boolean) JSONValue.parse(response.readEntity(String.class));
        } else {
            String error = response.readEntity(String.class);
            log.info("Error occured: " + error);
            throw new GitlabApiException(error);
        }

    }

    JSONObject postRequest(String resource, String jsonBody) throws GitlabApiException {
        WebTarget webTarget = getWebTarget(resource);
        String uri = getUriString(webTarget);
        log.info("Reqesting uri: " + uri + ", method=POST, body= " + jsonBody);

        Response response = webTarget.request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(jsonBody, MediaType.APPLICATION_JSON_TYPE));

        int responseStatus = response.getStatus();
        if (responseStatus == 201) {
            return response.readEntity(JSONObject.class);
        } else {
            String error = response.readEntity(String.class);
            log.info("Error occured: " + error);
            System.out.println(error);
            throw new GitlabApiException(error);
        }
    }

    JSONObject getRequest(String resource) throws GitlabApiException {
        WebTarget webTarget = getWebTarget(resource);
        String uri = getUriString(webTarget);
        log.info("Reqesting uri: " + uri + ", method=GET");

        Response response = webTarget.request(MediaType.APPLICATION_JSON_TYPE).get();

        int responseStatus = response.getStatus();
        if (responseStatus == 200) {
            return (JSONObject) JSONValue.parse(response.readEntity(String.class));
        } else {
            String error = response.readEntity(String.class);
            log.info("Error occured: " + error);
            throw new GitlabApiException(error);
        }
    }

    JSONArray getRequestArray(String resource) throws GitlabApiException {
        WebTarget webTarget = getWebTarget(resource);
        String uri = getUriString(webTarget);
        log.info("Reqesting uri: " + uri + ", method=GET");

        Response response = webTarget.request(MediaType.APPLICATION_JSON_TYPE).get();

        int responseStatus = response.getStatus();
        if (responseStatus == 200) {
            return (JSONArray) JSONValue.parse(response.readEntity(String.class));
        } else {
            String error = response.readEntity(String.class);
            log.info("Error occured: " + error);
            throw new GitlabApiException(error);
        }
    }

    JSONArray getRequestWithPaginationWithQuery(String resource, String query, String param) throws GitlabApiException {
        int page = 1;
        boolean done = false;
        JSONArray jsonArray = new JSONArray();

        while (!done) {
            WebTarget webTarget = getWebTarget(resource, page, query, param);
            String uri = getUriString(webTarget);
            log.info("Reqesting uri: " + uri + ", method=GET");
            Response response = webTarget.request(MediaType.APPLICATION_JSON_TYPE).get();

            if (response.getStatus() == 200) {
                @SuppressWarnings("unchecked")
                boolean b = jsonArray.addAll((JSONArray) JSONValue.parse(response.readEntity(String.class)));
                page++;
                if (response.getHeaderString("X-Next-Page").equals("")) {
                    done = true;
                }
            } else {
                String error = response.readEntity(String.class);
                log.info("Error occured: " + error);
                throw new GitlabApiException(error);
            }
        }
        return jsonArray;
    }

    JSONArray getRequestWithPagination(String resource) throws GitlabApiException {
        return getRequestWithPaginationWithQuery(resource, "", "");
    }

    private String getUriString(WebTarget wt) {
        return wt.getUri().toString().replaceAll("private_token\\=.*", "private_token=###########");
    }

    private WebTarget getWebTarget(String resourcePath) {
        WebTarget webTarget = client.target(getGitlabUrl()).path("/api/v3").path(resourcePath).queryParam("private_token", gitlabToken);
        return webTarget;
    }

    private WebTarget getWebTarget(String resourcePath, int page) {
        WebTarget webTarget = client.target(getGitlabUrl()).path("/api/v3").path(resourcePath).queryParam("page", page).queryParam("private_token", gitlabToken);
        return webTarget;
    }

    private WebTarget getWebTarget(String resourcePath, int page, String param, String query) {
        WebTarget webTarget = client.target(getGitlabUrl()).path("/api/v3").path(resourcePath).queryParam(param, query).queryParam("page", page).queryParam("private_token", gitlabToken);
        return webTarget;
    }

    public String getGitlabUrl() {
        String url = gitlabUrl;
        if (!url.startsWith("http://")) {
            url = "http://" + url;
        }
        return url;
    }
}
