#!/bin/bash

docker cp ldap-users ldap-service:/tmp/ldap-users

docker exec ldap-service ldapadd -cxD cn=admin,dc=example,dc=org -f /tmp/ldap-users/itunel.ldif -w admin
docker exec ldap-service ldapadd -cxD cn=admin,dc=example,dc=org -f /tmp/ldap-users/jnowak.ldif -w admin
docker exec ldap-service ldapadd -cxD cn=admin,dc=example,dc=org -f /tmp/ldap-users/kmarek.ldif -w admin
docker exec ldap-service ldapadd -cxD cn=admin,dc=example,dc=org -f /tmp/ldap-users/probak.ldif -w admin
docker exec ldap-service ldapadd -cxD cn=admin,dc=example,dc=org -f /tmp/ldap-users/kgadomski.ldif -w admin
docker exec ldap-service ldapadd -cxD cn=admin,dc=example,dc=org -f /tmp/ldap-users/anowacka.ldif -w admin
docker exec ldap-service ldapadd -cxD cn=admin,dc=example,dc=org -f /tmp/ldap-users/krychter.ldif -w admin
docker exec ldap-service ldapadd -cxD cn=admin,dc=example,dc=org -f /tmp/ldap-users/pszumanski.ldif -w admin
docker exec ldap-service ldapadd -cxD cn=admin,dc=example,dc=org -f /tmp/ldap-users/jbanachowicz.ldif -w admin
docker exec ldap-service ldapadd -cxD cn=admin,dc=example,dc=org -f /tmp/ldap-users/azegarska.ldif -w admin
docker exec ldap-service ldapadd -cxD cn=admin,dc=example,dc=org -f /tmp/ldap-users/ktarasiuk.ldif -w admin

